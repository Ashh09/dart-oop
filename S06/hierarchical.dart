void main() {
  Equipment equipment = new Equipment();
  equipment.name = 'Equipment-001';
  print(equipment.name);

  Loader loader = new Loader();
  loader.name = 'Loader-001';
  print(loader.name);
  print(loader.getCategory());
  
  Crane crane = new Crane();
  crane.name = 'Crane-001';
  print(crane.name);
  print(crane.getCategory());
}
class Equipment {
  String? name;
}
class Loader extends Equipment {
  String getCategory() {
    return '${this.name} is a loader.';
  }
}
class Crane extends Equipment {
  String getCategory() {
    return '${this.name} is a crane.';
  }
}