void main() {
  Person person = new Person(lastName: "Smith", firstName: "John");
  Employee employee = new Employee(lastName: "Westwood", firstName: "Jonas");

  print(person.getFullname());
  print(employee.getFullname());
}

class Person {
  String firstName;
  String lastName;

  Person({required this.firstName, required this.lastName});

  String getFullname() {
    return '${this.firstName} ${this.lastName}';
  }
}

class Employee extends Person {
  Employee({required String firstName, required String lastName})
      : super(firstName: firstName, lastName: lastName);
}
