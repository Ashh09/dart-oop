import './worker.dart';

void main(){

//Person personA = new Person(); 
Doctor doctor = new Doctor(firstName: "John", lastName: "Smith");
Carpenter carpenter = new Carpenter();
print(doctor.firstName + " " +  doctor.lastName);
print(carpenter.getType());

}

abstract class Person{
  
  //every class na implementation ni abstract ay dapat meron method na kagaya sakanya, and dapat may 
  //value or condition na pag dating sa concrete class
  String getFullname();
}

class Doctor implements Person{
  
  String firstName;
  String lastName;

  Doctor({
    required this.firstName,
    required this.lastName
  });
  String getFullname(){
    return "Dr. ${this.firstName} ${this.lastName}";
  }

}

