class _Worker{
  String _getType(){
    return "Worker";
  }
}

class Carpenter extends _Worker{
  String getType(){
    return super._getType();
  }
}