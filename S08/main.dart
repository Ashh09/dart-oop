void main() {
  Bulldozer bulldozer = new Bulldozer(
    equipmentType: "bulldozer",
    equipmentModel: "Catterpillar D10",
    equipmentBlade: "U blade"
  );
  TowerCrane towerCrane = new TowerCrane(
    equipmentType: "tower crane",
    equipmentModel: "370 EC-B 12 Fibre",
    equipmentHookRadius: 78,
    equipmentMaxCapacity: 12000
  );
  Loader loader = new Loader(
    equipmentType: "loader",
    equipmentModel: "Volvo L60H",
    loaderType: "Wheel loader",
    tippingLoad: 16530  
  );

  List<dynamic> equipmentList = [];
  equipmentList.add(bulldozer);
  equipmentList.add(towerCrane);
  equipmentList.add(loader);

  equipmentList.forEach((equipment) {
    print(equipment.describe());
  });
}
 
abstract class Equipment {
  String describe();
}

class Bulldozer implements Equipment {
  String equipmentType;
  String equipmentModel;
  String equipmentBlade;

  Bulldozer({
    required this.equipmentType,
    required this.equipmentModel,
    required this.equipmentBlade
  });

  String describe() {
    return "The ${this.equipmentType} ${this.equipmentModel} has a ${this.equipmentBlade}.";
  }
}

class TowerCrane implements Equipment {
  String equipmentType;
  String equipmentModel;
  num equipmentHookRadius;
  num equipmentMaxCapacity;

  TowerCrane({
    required this.equipmentType,
    required this.equipmentModel,
    required this.equipmentHookRadius,
    required this.equipmentMaxCapacity
  });

  String describe() {
    return "The ${this.equipmentType} ${this.equipmentModel} has a radius of ${this.equipmentHookRadius}m and max capacity of ${this.equipmentMaxCapacity}kg.";
  }
}

class Loader implements Equipment {
  String equipmentType;
  String equipmentModel;
  String loaderType;
  num tippingLoad;

  Loader({
    required this.equipmentType,
    required this.equipmentModel,
    required this.loaderType,
    required this.tippingLoad
  });

  String describe() {
    return 'The ${this.equipmentType} ${this.equipmentModel} is a ${this.loaderType} and has a tipping load of ${this.tippingLoad} lbs.';
  }
}
