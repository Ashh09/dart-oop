void main() {
  Function discountby25 = getDiscount(25);
  print(discountby25(1400));
  // String firstName = "John";
}

Function getDiscount(num percentage) {
  return (num amount) {
    return amount * percentage / 100;
  };
}
