//the main function is the entry of dart application
void main() {
  // print(getCompanyName());
  // print(getYearEstablished());
  // print(hasOnlineClasses());
  // print(getCoordinates());
  // print(combineAddress(
  //     "134 Timog Ave", "Sacred heart", "Quezon City", "Metro Manila"));

  // print(combineName('John', 'Smith'));
  // print(combineName('John', 'Smith', isLastNameFirst: true));
  // print(combineName('John', 'Smith', isLastNameFirst: false));
  // if no values are given, a default value can be assigned

  // List<String> persons = ["John Doe", "Jane Doe"];
  // List<String> students = ["John Doe", "Jane Doe"];

  //anonymous functions
  //persons.forEach((String person) {
  // print(person);
  //});

  //students.forEach((String person) {
  //print(person);
  // });

  //print name(value) is function execution/call/invocation.
  //printName is reference to the given function.
  // persons.forEach(printName);
  // students.forEach(printName);
}

void printName(String name) {
  print(name);
}

String combineName(String firstName, String lastName,
    {bool isLastNameFirst = false}) {
  if (isLastNameFirst) {
    return '$lastName $firstName';
  } else {
    return '$firstName $lastName';
  }
}

String combineAddress(
    String specifics, String barangay, String city, String province) {
  //return specifics + ',' + barangay + ',' + city + ',' + province;
  return '$specifics $barangay $city $province';
}

// String getCompanyName() {
//   return 'FFUF';
// }

// int getYearEstablished() {
//   return 2017;
// }

// bool hasOnlineClasses() {
//   return true;
// }

bool isUnderAge(age) => (age < 18) ? true : false;

Map<String, double> getCoordinates() {
  return {'latitude': 14.632702, 'longitude': 121.043716};
}
