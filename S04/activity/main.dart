void main() {
  List<num> values = [45, 34.2, 176.9, 32.2];
  num sum = 0;
  values.forEach((ex) => sum += ex);

  Function discountby20 = getDiscount(20);
  Function discountby40 = getDiscount(40);
  Function discountby60 = getDiscount(60);
  Function discountby80 = getDiscount(80);

  print(sum);
  print((sum - discountby20(sum)).toStringAsFixed(2));
  print((sum - discountby40(sum)).toStringAsFixed(2));
  print((sum - discountby60(sum)).toStringAsFixed(2));
  print((sum - discountby80(sum)).toStringAsFixed(2));
}

Function getDiscount(num percentage) {
  return (num amount) {
    return amount * percentage / 100;
  };
}
