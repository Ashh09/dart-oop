void main() {
  List<num> values = [45, 34.2, 176.9, 32.2];

  Function discountby20 = getDiscount(20);
  Function discountby40 = getDiscount(40);
  Function discountby60 = getDiscount(60);
  Function discountby80 = getDiscount(80);

  print(getTotalPrice(values));
  print((getTotalPrice(values) - discountby20(getTotalPrice(values)))
      .toStringAsFixed(2));
  print((getTotalPrice(values) - discountby40(getTotalPrice(values)))
      .toStringAsFixed(2));
  print((getTotalPrice(values) - discountby60(getTotalPrice(values)))
      .toStringAsFixed(2));
  print((getTotalPrice(values) - discountby80(getTotalPrice(values)))
      .toStringAsFixed(2));
  //print((sum - discountby40(sum)).toStringAsFixed(2));
  //print((sum - discountby60(sum)).toStringAsFixed(2));
  //print((sum - discountby80(sum)).toStringAsFixed(2));
}

Function getDiscount(num percentage) {
  return (num amount) {
    return amount * percentage / 100;
  };
}

num getTotalPrice(values, {sum = 0}) {
  values.forEach((ex) => sum += ex);
  return sum;
}
