import './models/user.dart';
void main(){
    // int x = 100;
    // int y = 100;
    // print(x.hashCode);
    // print(y.hashCode);
    // print(x == y);
    User userA = new User(id: 1, email: 'john@gmail.com');
    User userB = new User(id: 1, email: 'john@gmail.com');
    // print(userA.hashCode);
    // print(userB.hashCode);

    
    // To check for object  equality, we need to 
    // do it like the code below
    // bool isIdSame = userA.id == userB.id;
    // bool isEmailSame = userA.email == userB.email;
    // bool areObjectsSame = isIdSame && isEmailSame;



    // print(areObjectsSame);
    // print(userA.email);
    // userA.email = 'john@hotmail.com';
    // print(userA.email);
}
// Dart does not check the values of the object,
// but also its unique identifiers (through hash codes)
