import './freezed_models/user.dart';
void main(){
    User userA = new User(id: 1, email: 'john@gmail.com');
    User userB = new User(id: 1, email: 'john@gmail.com');
    // print(userA.hashCode);
    // print(userB.hashCode);
    // print(userA == userB);
    //Demonstration of object immutability below
    //Immutability means changes are not allowed
    //Mutable and immutable
    //It ensures that an object will not be changed aaccidentally
    //Instead of directly changing the object property
    //the object itself will be changed or re-assigned with new value
    // To achieve this, we use the object.copyWith*( method)
    print(userA.email);
    userA = userA.copyWith(email: 'john@hotmail.com');
    print(userA.email);
    var response = {'id' : 3, 'email' : 'doe@gmail.com'};
    // User userC = User(
    //     id: response['id'] as int, 
    //     email: response['email'] as String
    // );
    User userC = User.fromJson(response);
    print(userC);
    print(userC.toJson());
}
// Dart does not check the values of the object,
// but also its unique identifiers (through hash codes)