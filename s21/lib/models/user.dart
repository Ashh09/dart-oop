class User {
    late final int? id;
    late final String? email;
    late final String? accessToken;
    User({
        this.id,
        this.email,
        this.accessToken
    });
}