class Note {
    late final int id;
    late final int userId;
    late final String title;
    late final String description;
    Note({
        required this.id,
        required this.userId,
        required this.title,
        required this.description
    });
}