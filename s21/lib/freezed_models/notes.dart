import 'package:freezed_annotation/freezed_annotation.dart';

part 'notes.freezed.dart';
part 'notes.g.dart';

@freezed
class Notes with _$Notes {
    const factory Notes({
        required int id,
        required int userId,
        required String title,
        required String description,
    }) = _Notes;

    factory Notes.fromJson(Map<String, dynamic> json ) => _$NotesFromJson(json);
}
