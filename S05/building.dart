class Building {
  String? _name;
  int floors;
  String address;
  Building(this._name,
      { //required is needed for optional
      required this.floors,
      required this.address}) {
    print('a building object has been created');
  }

  String? get Name {
    print("The building\'s name has been retrieved");
    return this._name;
  }

  void set Name(String? name) {
    print("The building\'s name has been changed to $name");
    this._name = name;
  }

  Map<String, dynamic> getProperties() {
    return {'name': this._name, 'floors': this.floors, 'address': this.address};
  }
}
