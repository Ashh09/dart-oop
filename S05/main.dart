import './building.dart';

void main() {
  Building building = new Building('Caswynn',
      address: '134 Timog ave. brgy. sacred heart, Quezon City, Metro Manila',
      floors: 8);

  // print(building);
  // print(building.name);
  // print(building.floors);
  // print(building.address);
  // print(building.getProperties());

  building.Name = 'GEMPC';
  print(building.Name);
}

// class Building {
//   String? _name;
//   int floors;
//   String address;
//   Building(this._name,
//       { //required is needed for optional
//       required this.floors,
//       required this.address}) {
//     print('a building object has been created');
//   }

//   String? get Name {
//     return this._name;
//   }

//   void set Name(String? name) {}

//   Map<String, dynamic> getProperties() {
//     return {'name': this._name, 'floors': this.floors, 'address': this.address};
//   }
// }
