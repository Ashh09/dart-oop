//BARE FORM CLASSES-
void main() {
  Building building = new Building(
      name: "Caswynn",
      floors: 8,
      address:
          '134 Timog Avenue, Brgy. Sacred Heart, Quezon City, Metro Manila');
  // building.name = "Caswynn";
  // building.floors = 8;
  // building.address =
  //     '134 Timog Avenue, Brgy. Sacred Heart, Quezon City, Metro Manila';

  //print(building);
  print(building.name);
  print(building.floors);
  print(building.address);
}

class Building {
  String name;
  int floors;
  String address;

  Building({required this.name, required this.floors, required this.address}) {
    print('A building project has been created');
  }
}

//Before addressing the nullable value
// class Building {
//   String name;
//   int floors;
//   String address;
//   List<String> rooms;

//   Building(this.name, this.floors, this.address, this.rooms) {
//     print("A building object has been created");
//   }
// }
//Before addressing the nullable fields.
// class Building {
//   String? name;
//   int? floors;
//   String? address;

//   Building(String name, int floors, String address) {
//     this.name = name;
//     this.floors = floors;
//     this.address = address;
//   }
// }
