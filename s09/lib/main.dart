import 'package:flutter/material.dart';
import './body_questions.dart';
import './body_answers.dart';
void main() {
    //The App() is the root widget.
    runApp(App());
}
//Stateless Widget = nothing will be changed
class App extends StatefulWidget{
  @override
  AppState createState() => AppState();
}

class AppState extends State<App>{
    int questionIdx = 0;

    var answers = [];
    bool showAnswers = false;
    final questions = [
        {
            'question': 'What is the nature of your business needs?',
            'options' : ['Time Tracking', 'Asset Management', 'Issue Tracking']
        },
        {
            'question': 'What is the expected size of the user base?',
            'options' : ['Less than 1,000', 'Less than 10,000', 'More than 10,000']
        },
        {
            'question': 'In which region would the majority of the user base be?',
            'options' : ['Asia', 'Europe', 'Americas','Africa', 'Middle East']
        },
        {
            'question': 'What is the expected project duration?',
            'options' : ['Less than 3 months', '3-6 months', '6-9 months', '9-12 months', 'more than 12 months']
        }
    ];

    void nextQuestion(String? answer ) {
        answers.add({
            'question' :questions[questionIdx]['question'],
            'answer' : (answer == null)? '' : answer
        });
        //print(answers);

        if(questionIdx < questions.length-1){
            setState(() => questionIdx++);
        }else{
            setState(() => showAnswers = true);
        }
        
    }

    @override
    Widget build(BuildContext context) {
        var bodyQuestions = BodyQuestions(questions: questions, questionIdx: questionIdx, nextQuestion: nextQuestion,);
            var bodyAnswers = BodyAnswers(answers : answers);

        Scaffold homepage = Scaffold(
            appBar: AppBar(title: Text('Homepage')),
            body: (showAnswers) ? bodyAnswers : bodyQuestions
        );

        return MaterialApp(
            home: homepage
            //home: Text('Hello World')
        );
        /*
        Widget Tree:
        MaterialApp
        Scaffold
        AppBar
        Text
        Container
        Text
        */
    }
}

//The invisible widgets are container and scaffold
        //the visible widgets are AppBar and Text

        //To specfy a margin or padding spacing we can use edgeinsets.
        //The values for spacing are in multiples of 8
        //To add spacing on all sides, use edgeinsets.all().
        //to add spacing on certain sides, use edgeinsets.only(direction: value)
        
        //in flutter,width = double.infinity is equivalent to width = 100% in CSS
        //to put coilors in a container, the decoration: BoxDecoration(color: Colors.green) can be used

        //In a column widget, the main axis alignmewnt is vertical (or from top to botton)
        //to change the horizontal alignment of widgets in a column, we must use crossAxisAlignment
        //for example, if we want to place column widgets to the horizontal left, we set CrossAxisAlignment.start
        //In the context of the column we can consider the column as vertical, and its cross axis as horizontal
        

        //The scaffold widget provides basic design layout structure.
        //The scaffold can be given UI elements such as an app bar

        //the state is lifted up
        //App.questionIdx -> BodyQuestions.questionIdx