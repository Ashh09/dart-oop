import 'package:flutter/material.dart';
import './answer_button.dart';
//datas from AppState is shared with this widget
class BodyQuestions extends StatelessWidget {
    final questions;
    final int questionIdx;
    final Function nextQuestion;

    BodyQuestions({
        required this.questions,
        required this.questionIdx,
        required this.nextQuestion
    });

    @override
    Widget build(BuildContext context) {
        Text questionText = Text(
            questions[questionIdx]['question'].toString(),
            style: TextStyle(
                fontSize: 24.0
            )
        );

        //loops should be here
        var options = questions[questionIdx]['options'] as List<String>;

        //Before map() ['Time Tracking', 'Asset Management', 'Issue Tracking']
        //After map() [AnswerButton('Time Tracking),AnswerButton('Asset Management),AnswerButton('Issue Tracking)]
    
        var answerOptions = options.map((String option){
            return AnswerButton(text: option, nextQuestion: nextQuestion);
        });

        return Container(
            width: double.infinity,
            padding: EdgeInsets.all(16),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                //mainAxisAlignment: MainAxisAlignment.center,
                //[questionText,[answerOptions], skipQuestion]
                //[questiontext, ...answerOptions, skipQuestion]
                ///...converts the list to [questionText, answerOptionsA, answerOptionsB, answerOptionsN, skipQuestion]
                //... = spread operator
                children: [
                    questionText,
                    ...answerOptions,
                    Container(
                        margin: EdgeInsets.only(top: 30),
                        width: double.infinity,
                        child: ElevatedButton(
                            child: Text('Skip Question'),
                            onPressed: () => nextQuestion(null)

                            //onPressed is an eventlistener 
                            //no() for it to be executed later
                        )
                    )
                ]
            )
        );
    }
}   