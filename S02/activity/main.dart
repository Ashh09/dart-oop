void main() {
  const Map<String, dynamic> details = {
    "First Name ": "John",
    "Last Name": "Smith",
    "Age": 34,
  };

  Set<String> country = {
    "Philippines",
    "Canada",
    "Singapore",
    "Japan",
    "Thailand",
    "United Kingdom"
  };

  List<String> processor = [
    "Ryzen 3 3200G",
    "Ryzen 5 3600",
    "Ryzen 7 5800X",
    "Ryzen 9 5950X"
  ];

  print(details);
  print(country);
  print(processor);

  print(details.length);
  print(country.length);
  print(processor.length);
}
