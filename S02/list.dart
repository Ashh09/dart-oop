void main() {
  List<int> discountRanges = [20, 40, 60, 80];
  List<String> names = ["John", "Jane", "Tom"];

  const List<String> maritalStatus = [
    "single",
    "married",
    "divorced",
    "widowed"
  ];

  print(discountRanges);
  print(names);

  print(discountRanges[2]);
  print(names[0]);

  print(discountRanges.length);
  print(names.length);

  names[0] = "Jonathan";
  print(names);

  names.add("Ash");
  names.insert(0, "Mark");
  print(names);

  //These are properties
  print(names.isEmpty);
  print(names.isNotEmpty);
  print(names.first);
  print(names.last);
  print(names.reversed);

  //This is a method
  names.sort((a, b) => a.length.compareTo(b.length));

  //names.sort();
  print(names);
  print(names.reversed);
}
