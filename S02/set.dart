void main() {
  Set<String> subscontractors = {"Sonderhoff", "Stahlschmidt"};
  subscontractors.add('Schweisstechnik');
  subscontractors.add('Kreatel');
  subscontractors.add('Kunstoffe');
  subscontractors.remove('Sonderhoff');
  print(subscontractors);
}
