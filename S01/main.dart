import 'dart:ffi';

void main() {
  String name = 'John';
  String? middleName = null;
  String lastname = 'Smith';
  int age = 31;
  double height = 172.45;
  num weight = 65.3;
  bool isRegistered = false;
  List<num> grades = [98.2, 89, 87.88, 91.2]; // List = array []

  Map<String, dynamic> personA = {'name': 'Brandon', 'batch': 213};
  Map personB = new Map();
  personB['name'] = 'Juan';
  personB['batch'] = 89;

  //with final once the value is set, it cannot be changed
  final DateTime now = DateTime.now();
  //now = DateTime.now();

  //with const, an identifier must have a corresponding declaration of value.
  const String companyAcronym = 'FFUF';
  //companyAcronym = 'FFUF';

  //print(name + ' ' + lastname);
  print('Name: $name $lastname');
  print('Age' + age.toString());
  print('Height: ' + height.toString());
  print('Weight: ' + weight.toString());
  print('Registered: ' + isRegistered.toString());
  print('Grades: ' + grades.toString());
  print('Current Date time: ' + now.toString());

  print(['name']);
}
